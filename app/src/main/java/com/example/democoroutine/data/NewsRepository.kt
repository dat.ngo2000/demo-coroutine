package com.example.democoroutine.data

import com.example.democoroutine.data.models.ArticleModel

interface NewsRepository {
    suspend fun getBreakingNews() : Result<@JvmSuppressWildcards List<ArticleModel>>
    suspend fun searchArticle(query: String): Result<@JvmSuppressWildcards List<ArticleModel>>
}