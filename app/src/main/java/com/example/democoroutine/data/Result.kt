package com.example.democoroutine.data

sealed class Result<out T> {
    data class Success<T>(val value: T): Result<T>()
    data class Failure(val cause: String): Result<Nothing>()
}
