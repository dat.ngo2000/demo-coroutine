package com.example.democoroutine.data.network

import com.example.democoroutine.data.models.ArticleModel

data class NewsResponse (
    val status: String,
    val articles: List<ArticleModel>,
    val totalResults: Int
        )