package com.example.democoroutine.data

import com.example.democoroutine.data.models.ArticleModel
import com.example.democoroutine.data.network.NewsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val newsApi: NewsApi
) : NewsRepository {

    override suspend fun getBreakingNews() = withContext(Dispatchers.IO) {
        try {
            Result.Success(newsApi.getBreakingNews().articles)
        } catch (e: Exception) {
            Result.Failure(e.toString())
        }
    }

    override suspend fun searchArticle(query: String) = withContext(Dispatchers.IO)  {
        try {
            Result.Success(newsApi.searchNews(q = query).articles)
        } catch (e: Exception) {
            Result.Failure(e.toString())
        }
    }

}