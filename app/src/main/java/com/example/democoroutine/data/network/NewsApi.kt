package com.example.democoroutine.data.network

import retrofit2.http.*

const val API_KEY = "29d57684f98b468187b46c9e94a89e34"

interface NewsApi {

    @GET("/v2/top-headlines")
    suspend fun getBreakingNews(
        @Query("country")
        country: String = "us",
        @Query("apiKey")
        apiKey: String = API_KEY
    ) : NewsResponse

    @GET("/v2/everything")
    suspend fun searchNews(
        @Query("sortBy")
        sortBy: String = "popularity",
        @Query("from")
        from: String = "2022-02-16",
        @Query("apiKey")
        apiKey: String = API_KEY,
        @Query("q")
        q: String
    ) : NewsResponse
}