package com.example.democoroutine.di

import com.example.democoroutine.data.NewsRepository
import com.example.democoroutine.data.NewsRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {
    @Singleton
    @Binds
    abstract fun getNewsRepository(repository: NewsRepositoryImpl): NewsRepository
}