package com.example.democoroutine.di

import com.example.democoroutine.data.network.NewsApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.*

const val BASE_URL = "https://newsapi.org"

@Module
class NetworkModule {
    @Provides
    fun getRetrofit(): NewsApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NewsApi::class.java)
    }
}