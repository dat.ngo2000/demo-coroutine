package com.example.democoroutine.di

import android.app.Activity
import com.example.democoroutine.View.HeadLineFragment
import com.example.democoroutine.View.SearchFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {
    fun inject(fragment: HeadLineFragment)
    fun inject(fragment: SearchFragment)
}