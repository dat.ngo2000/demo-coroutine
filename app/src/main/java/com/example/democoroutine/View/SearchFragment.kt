package com.example.democoroutine.View

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.democoroutine.R
import com.example.democoroutine.View.Adapter.ArticleAdapter
import com.example.democoroutine.ViewModel.HeadLineViewModel
import com.example.democoroutine.ViewModel.SearchViewModel
import com.example.democoroutine.data.models.ArticleModel
import com.example.democoroutine.databinding.FragmentHeadLineBinding
import com.example.democoroutine.databinding.FragmentSearchBinding
import com.example.democoroutine.di.DaggerAppComponent
import kotlinx.android.synthetic.main.fragment_head_line.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchFragment : Fragment() {


    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: ArticleAdapter


    @Inject
    lateinit var searchViewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    private fun getData(q: String) {
        if(q.isEmpty()) return
        searchViewModel.searchArticle(q)
    }

    private fun setUpView() {
        adapter = ArticleAdapter(mutableListOf())
        setUpRecyclerView(adapter)
        observeUpdate(adapter)
        setUpEditTextChange()
    }

    private fun setUpEditTextChange() {
        etSearch.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                getData(p0.toString())
            }

            override fun afterTextChanged(p0: Editable?) {
            }

        })
    }

    private fun setUpRecyclerView(articleAdapter: ArticleAdapter){
        binding.rvSearchNews.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter =  articleAdapter
        }
    }

    private fun observeUpdate(articleAdapter: ArticleAdapter) {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            searchViewModel.articles.collectLatest {
                updateUi(it, articleAdapter)
            }
        }

    }

    private fun updateUi(articles: List<ArticleModel>, articleAdapter: ArticleAdapter){
        articleAdapter.updateData(articles)
    }

    override fun onAttach(context: Context) {

        DaggerAppComponent.create().inject(this)

        super.onAttach(context)
    }

}