package com.example.democoroutine.View

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.democoroutine.R
import com.example.democoroutine.View.Adapter.ArticleAdapter
import com.example.democoroutine.ViewModel.HeadLineViewModel
import com.example.democoroutine.data.models.ArticleModel
import com.example.democoroutine.databinding.FragmentHeadLineBinding
import com.example.democoroutine.di.DaggerAppComponent
import kotlinx.android.synthetic.main.fragment_head_line.*
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject


class HeadLineFragment : Fragment(){


    private var _binding: FragmentHeadLineBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: ArticleAdapter

    @Inject
    lateinit var headLineViewModel: HeadLineViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHeadLineBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
        getData()
    }

    private fun getData() {
        headLineViewModel.getHeadLineNews()
    }

    private fun setUpView() {
        adapter = ArticleAdapter(mutableListOf())
        swipeLayout.isRefreshing = true
        setUpRecyclerView(adapter)
        observeUpdate(adapter)
        setUpRefreshLayout()
    }

    private fun setUpRefreshLayout() {
        swipeLayout.setOnRefreshListener {
            getData()
            swipeLayout.isRefreshing = false
        }
    }

    private fun setUpRecyclerView(articleAdapter: ArticleAdapter){
        binding.rvHeadLineNews.apply {
            layoutManager = LinearLayoutManager(requireContext())
           adapter =  articleAdapter
        }
    }

    private fun observeUpdate(articleAdapter: ArticleAdapter) {

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            headLineViewModel.articles.collectLatest {
                updateUi(it, articleAdapter)
                swipeLayout.isRefreshing = false
            }
        }

    }

    private fun updateUi(articles: List<ArticleModel>, articleAdapter: ArticleAdapter){
        articleAdapter.updateData(articles)
    }

    override fun onAttach(context: Context) {

        DaggerAppComponent.create().inject(this)

        super.onAttach(context)
    }




}