package com.example.democoroutine.View.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.democoroutine.R
import com.example.democoroutine.data.models.ArticleModel
import kotlinx.android.synthetic.main.news_item.view.*

class ArticleAdapter(private var articles: MutableList<ArticleModel>): RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {


    class ArticleViewHolder(private val viewItem: View): RecyclerView.ViewHolder(viewItem) {
        fun binding(article: ArticleModel) {
            viewItem.tvTitle.text = article.title
            viewItem.tvAuthor.text = article.author
            viewItem.tvDescription.text = article.description
            Glide.with(viewItem).load(article.urlToImage).into(viewItem.imgNews)
        }
    }

    fun updateData(newData: List<ArticleModel>) {
        articles.clear()
        articles.addAll(newData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.news_item, parent, false)
        return ArticleViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.binding(articles[position])
    }

    override fun getItemCount(): Int {
        return articles.size
    }
}