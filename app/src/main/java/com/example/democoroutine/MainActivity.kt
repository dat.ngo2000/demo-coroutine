package com.example.democoroutine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.democoroutine.View.HeadLineFragment
import com.example.democoroutine.View.SearchFragment
import com.example.democoroutine.di.AppComponent
import com.example.democoroutine.di.DaggerAppComponent
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        val headLineFragment = HeadLineFragment()
        val searchFragment = SearchFragment()

        setFragment(headLineFragment)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)

        bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId) {
                R.id.news->setFragment(headLineFragment)
                else -> setFragment(searchFragment)
            }
            true
        }
    }

    private fun setFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.host_fragment, fragment)
            commit()
        }
}