package com.example.democoroutine.ViewModel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.democoroutine.data.NewsRepository
import com.example.democoroutine.data.Result
import com.example.democoroutine.data.models.ArticleModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class SearchViewModel @Inject constructor(private val newsRepository: NewsRepository): ViewModel() {

    private var _articles = MutableStateFlow<List<ArticleModel>>(emptyList<ArticleModel>())

    var articles: StateFlow<List<ArticleModel>> = _articles.asStateFlow()

    fun searchArticle(query: String) {

        viewModelScope.launch {
            when(val result = newsRepository.searchArticle(query)){
                is Result.Success -> handleSuccess(result.value)
                is Result.Failure ->  handleFailures(result.cause)
            }
        }

    }

    private fun handleFailures(cause: String) {
        Log.d(ERROR, cause)
    }

    private fun handleSuccess(articles: List<ArticleModel>){
        _articles.value = articles
    }

}